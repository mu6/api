# Mu6

## Dependencies

  - JRE 1.8
  - Maven

## First Time Setup

```
git clone git@gitlab.com:mu6/api.git
cd api
mvn package
java -jar target/mu6.jar
```

## Subsequent Development
```
cd api
git pull
mvn package
java -jar target/mu6.jar

```

## Testing
Remember to replace `localhost:4567` with `https://api.mu6.xyz` if testing the server.

```
 curl -i -X POST -H "Content-Type: application/json" -d "{'user':{'fname':'Sean','lname':'Milligan','email':'sean@gmail.com'},'credentials':{'email': 'sean@gmail.com', 'password':'abc123'}}" localhost:4567/user/add
```


## Connecting to the Database Provider

You will need the following credentials:
```
USERNAME: rjhilbert
PASSWORD: 110677136
```

```
mysql -u USERNAME -h mysql2.cs.stonybrook.edu --skip-ssl -p

```

The mysql program will prompt for the password on the next line.


### Non-school database (AWS MySQL 5.7.19)
```
HOST mu6.cuvimpuwlida.us-west-2.rds.amazonaws.com
PORT 3306
USER mu6
PASS password123

mysql -u mu6 -h mu6.cuvimpuwlida.us-west-2.rds.amazonaws.com -P 3306 -p
```

### Change to Database

You will need the following database name (depending on dev/production):
```
database_name: rjhilbert, semilligan, mu6, etc.
```

```
USE database_name
```

### List tables
```
SHOW TABLES;
```

### View Table Structure
```
DESCRIBE table_name;
```

### View Table
```
SELECT * FROM table_name;
```

### View User Table without Password or Salt
```
select id, date, title, accountType, accountName, accountNumber, routingNumber, email, fname, image, lname, ccnum, city, cvv, expDate, state, street1, street2, zip, library_id from User;
```

## Testing multipart/form-data

  1. Start with a fresh database and make an artist.

```
curl -i -X POST -H "Content-Type: application/json" -H "Accept: application/json" -d "{'user':{'accountType':'ARTIST','fname':'Sean','lname':'Milligan','email':'sean@gmail.com'},'credentials':{'email': 'sean@gmail.com', 'password':'abc123'}}" localhost:4567/user/add
```

  2. Get a random jpg/jpeg in the same folder where you're curling from. Here I call it `bridge.jpg` Make sure the artistId matches the userId for the artist you just created. This may be `1` if there is no default root user, or `2` if there is.

```
curl -i -X POST -H "Content-Type: multipart/form-data" -H "Accept: application/json" -F "metadata={'title':'Bridge Over Troubled Water', 'artistId':2, 'releaseDate':'1970-01'}" -F "albumArt=@bridge.jpg" localhost:4567/album/add
```

  3. Check that a folder called `files.mu6.xyz` was created as a sibling directory to the directory that contains the Mu6 API project, and that the file is in there.

Note: there _shouldn't_ be any permission errors when this is run on the server as the `mu6.jar` file is executed under the Ubuntu user and the folder made should be `/home/Ubuntu/files.mu6.xyz/`. But, if errors arise, do investigate ownership permissions. Also check if the folder already existing (or not already existing) is causing the problem. To view ownership do `ls -l` and to recursively assign ownership of a directory and subdirs to a user, do `chown -R username:username directory` -- so specifically this would be `chown -R Ubuntu:Ubuntu ~/files.mu6.xyz/`