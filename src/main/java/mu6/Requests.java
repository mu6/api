package mu6;

import spark.*;
import spark.utils.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
import static mu6.Json.fromJson;

public interface Requests{
	String URL  = "https://files.mu6.xyz/";
	String PATH = "../files.mu6.xyz/";
	static Metadata processMultipartRequest(Request request){
		request.attribute("org.eclipse.jetty.multipartConfig",new MultipartConfigElement(PATH));
		try{
			Metadata data;
			Part infoPart = request.raw().getPart("metadata");
			try(InputStream in = infoPart.getInputStream()){
				data = fromJson(IOUtils.toString(in),Metadata.class);
			}
			Part filePart = request.raw().getPart("data");
			final byte[]bytes;
			try(InputStream in=filePart.getInputStream();ByteArrayOutputStream out=new ByteArrayOutputStream()){
				IOUtils.copy(in,out);
				bytes = out.toByteArray();
			}
			String filename = Integer.toString(Arrays.hashCode(bytes),Character.MAX_RADIX);
			data.path = URL+filename;
			try(OutputStream out=new FileOutputStream(PATH+filename);InputStream in=new ByteArrayInputStream(bytes)){
				IOUtils.copy(in,out);
			}
			return data;
		}catch(Exception e){
			return new Metadata();
		}
	}
	static int getId(Request request){
		try{
			return Integer.parseInt(request.params(":id"));
		}catch(NumberFormatException e){
			return 0;
		}
	}
	static int getId(Request request,char c){
		try{
			return Integer.parseInt(request.params(":"+c+"id"));
		}catch(NumberFormatException e){
			return 0;
		}
	}
	static int getCurrentUID(Request request){
		try{
			return request.session().attribute("UID");
		}catch(NumberFormatException e){
			return 0;
		}
	}
}