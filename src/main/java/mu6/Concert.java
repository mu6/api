package mu6;

import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import java.lang.reflect.Type;
import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_CREATED;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import spark.Request;
import spark.Response;
import static mu6.Requests.processMultipartRequest;
import static spark.Spark.halt;

@Entity public class Concert extends DatabaseEntity<Concert>{

	@NotNull User artist = new User();
	@NotNull String venue = "";
	@NotNull String description = "";
	@NotNull String image = "";

	@Override public JsonObject serialize(Concert concert,Type type,JsonSerializationContext context) {
		JsonObject json = super.serialize(concert,type,context);
		json.addProperty("venue", concert.venue);
		json.addProperty("details",concert.description);
		json.addProperty("artistId", concert.artist.id);
		json.addProperty("image", concert.image);
		return json;
	}
	public static Concert find(int id){
		return find(Concert.class,id);
	}
	public static Concert find(Request request,Response response){
		return find(Concert.class,request,response);
	}
	public static Concert create(Request request, Response response){
		Metadata data = processMultipartRequest(request);
		User artist = User.find(data.artistId);
		Concert concert = new Concert();
		concert.artist = artist;
		concert.date = data.date;
		concert.description = data.description;
		concert.title = data.title;
		concert.venue = data.venue;
		concert.image = data.path;
		if(!concert.persist()) throw halt(HTTP_BAD_REQUEST);
		response.status(HTTP_CREATED);
		return concert;
	}
}
