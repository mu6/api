package mu6;

import com.google.gson.*;
import spark.*;
import java.lang.reflect.*;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import static java.net.HttpURLConnection.*;
import static mu6.Json.*;
import static mu6.Requests.processMultipartRequest;
import static spark.Spark.halt;

@Entity public class Playlist extends DatabaseEntity<Playlist>implements List<Track>{
	public static Playlist find(int id){
		return DatabaseEntity.find(Playlist.class,id);
	}
	public static Playlist find(Request request,Response response){//returns playlist information
		return find(Playlist.class,request,response);
	}
	public static Playlist create(Request request,Response response){
		Metadata data = processMultipartRequest(request);
		User user = User.find(data.artistId);
		if(user==null) throw halt(HTTP_BAD_REQUEST);
		Playlist playlist = new Playlist(user);
		playlist.title = data.title;
		playlist.description = data.description;
		playlist.image = data.path;
		playlist.date = data.date;
		if(!persist(playlist).committed()) throw halt(HTTP_CONFLICT);
		response.status(HTTP_CREATED);
		return playlist;
	}
	public static String getTracks(Request request,Response response){//returns array of playlist songs
		response.status(HTTP_NOT_FOUND);
		try(Playlist playlist = find(Requests.getId(request))){
			String json = toJson(playlist.tracks);
			response.status(HTTP_OK);
			return json;
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String addTrack(Request request,Response response){
		response.status(HTTP_NOT_FOUND);
		int lid = Requests.getId(request,'l');
		int tid = Requests.getId(request,'t');
		try(Playlist list=find(lid);Track track=Track.find(tid)){
			list.add(track);
			list.merge();
			response.status(HTTP_OK);
			return"";
		}catch(Exception e){
			e.printStackTrace();
			return"";
		}
	}
	public static String removeTrack(Request request,Response response){
		response.status(HTTP_NOT_FOUND);
		int lid = Requests.getId(request,'l');
		int tid = Requests.getId(request,'t');
		try(Playlist list=find(lid);Track track=Track.find(tid)){
			list.remove(track);
			list.merge();
			response.status(HTTP_OK);
			return"";
		}catch(RuntimeException e){e.printStackTrace();
			return"";
		}
	}
	public static String update(Request request, Response response){
		response.status(HTTP_NOT_FOUND);
		Metadata data = processMultipartRequest(request);
		int lid = Requests.getId(request,'l');
		try(User user = User.find(data.artistId)){
			Playlist newList = new Playlist(user);
			newList.title = data.title;
			newList.description = data.description;
			newList.image = data.path;
			newList.date = data.date;
			try(Playlist oldList = find(lid)){
				/*
				if(!oldList.compare(newList)){
					oldList.update(newList);
					oldList.merge();
					response.status(HTTP_OK);
					return"";
				}
					response.status(HTTP_OK);
					return"";
				*/
			}catch(RuntimeException e){
				return"";
			}
			response.status(HTTP_OK);
			return"";
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String delete(Request request, Response response){
		response.status(HTTP_NOT_FOUND);
		int lid = Requests.getId(request, 'l');
		try(Playlist list = find(lid)){
			list.remove();
			response.status(HTTP_OK);
			return"";
		}catch(RuntimeException e){
			return"";
		}
	}
	@NotNull protected boolean privacy;
	@NotNull protected String title = "";
	@NotNull protected String description = "";
	@NotNull protected String image = "";
	@NotNull @ManyToOne protected User owner;
	@NotNull @OneToMany Set<User> followers = new HashSet<>();
	@NotNull @ManyToMany @OrderColumn protected List<Track> tracks = new ArrayList<>();
	public Playlist(){
		owner = new User();
	}
	public Playlist(User user){
		owner = user;
	}
	public Set<User> getFollowers(){
		return Collections.unmodifiableSet(followers);
	}
	public boolean getPrivacy() {
		return privacy;
	}
	public void setPrivacy(boolean privacy) {
		this.privacy = privacy;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override public JsonObject serialize(Playlist playlist,Type type,JsonSerializationContext context){
		JsonObject json = super.serialize(playlist,type,context);
		json.addProperty("title",  playlist.title);
		json.addProperty("image",  playlist.image);
		json.addProperty("privacy",playlist.privacy);
		json.addProperty("listId", playlist.id);
		json.addProperty("ownerId",playlist.owner.id);
		json.addProperty("description",playlist.description);
		return json;
	}
	//BOILERPLATE LIST DELEGATION METHODS BELOW
	@Override public int size(){return tracks.size();}
	@Override public boolean isEmpty(){return tracks.isEmpty();}
	@Override public boolean contains(Object o){return tracks.contains(o);}
	@Override public Iterator<Track>iterator(){return tracks.iterator();}
	@Override public Object[]toArray(){return tracks.toArray();}
	@Override public<T>T[]toArray(T[]t){return tracks.toArray(t);}
	@Override public boolean add(Track track){return tracks.add(track);}
	@Override public boolean remove(Object o){return tracks.remove(o);}
	@Override public boolean containsAll(Collection<?>collection){return tracks.containsAll(collection);}
	@Override public boolean addAll(Collection<?extends Track>collection){return tracks.addAll(collection);}
	@Override public boolean addAll(int i,Collection<?extends Track>collection){return tracks.addAll(i,collection);}
	@Override public boolean removeAll(Collection<?>collection){return tracks.removeAll(collection);}
	@Override public boolean retainAll(Collection<?>collection){return tracks.retainAll(collection);}
	@Override public void clear(){tracks.clear();}
	@Override public Track get(int i){return tracks.get(i);}
	@Override public Track set(int i,Track track){return tracks.set(i,track);}
	@Override public void add(int i,Track track){tracks.add(i,track);}
	@Override public Track remove(int i){return tracks.remove(i);}
	@Override public int indexOf(Object o){return tracks.indexOf(o);}
	@Override public int lastIndexOf(Object o){return tracks.lastIndexOf(o);}
	@Override public ListIterator<Track>listIterator(){return tracks.listIterator();}
	@Override public ListIterator<Track>listIterator(int i){return tracks.listIterator(i);}
	@Override public List<Track> subList(int i,int j){return tracks.subList(i,j);}
}