package mu6;

import javax.persistence.*;
import javax.validation.constraints.*;

@Embeddable public class BankAccount{
	@NotNull private String accountName = "";
	private long accountNumber;
	private int routingNumber;

	public String getAccountName(){
		return accountName;
	}
	public long getAccountNumber(){
		return accountNumber;
	}
	public long getRoutingNumber(){
		return routingNumber;
	}
}
