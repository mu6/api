package mu6;

import javax.validation.constraints.*;

import javax.persistence.*;
import java.sql.*;

@Embeddable public class Subscription{
	@NotNull protected String ccnum = "";
	@NotNull protected Date expDate = new Date(System.currentTimeMillis());
	@NotNull @Pattern(regexp="^[0-9]{3,4}$")protected String cvv = "";
	@NotNull protected String street1 = "";
	@NotNull protected String street2 = "";
	@NotNull protected String city = "";
	@NotNull protected String state = "";
	@NotNull @Pattern(regexp="^[0-9]{5}(-[0-9]{4})?$")protected String zip;
}