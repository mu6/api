package mu6;

import com.google.gson.*;
import spark.*;
import javax.validation.constraints.*;
import javax.persistence.*;
import java.lang.reflect.*;
import java.util.*;
import static java.net.HttpURLConnection.*;
import static javax.persistence.CascadeType.*;
import static mu6.Credentials.hash;
import static mu6.Credentials.newSalt;
import static mu6.Json.*;
import static spark.Spark.halt;

@Entity 
public class User extends DatabaseEntity<User>{
	public static enum AccountType{
		BASIC,PREMIUM,ARTIST,ADMIN
	}
	public static class Info{//used for deserializing new user creation requests
		public User user = new User();
		public Credentials credentials = new Credentials();
		public Subscription subscription = new Subscription();
		public BankAccount bankAccount = new BankAccount();
	}
	public static class Password{
		public String oldPassword;
		public String newPassword;
	}
	@NotNull AccountType accountType = AccountType.BASIC;
	@NotNull @Email @Column(unique=true)protected String email = "";
	@NotNull String fname = "";
	@NotNull String lname = "";
	@NotNull String salt = "";
	@NotNull String passwordHash = "";
	@NotNull @OneToOne(cascade={PERSIST, REMOVE})protected Playlist library = new Playlist(this);
	@NotNull @OneToMany Set<Playlist> playlists = new HashSet<>();
	@NotNull @ManyToMany Set<User> followers = new HashSet<>();
	@NotNull @ManyToMany Set<User> following = new HashSet<>();
	@NotNull BankAccount bankAccount = new BankAccount();//admins and artists
	@NotNull Subscription subscription = new Subscription();//premium users
	@NotNull String image = "";//artists
	@NotNull boolean isBanned = false;
	@NotNull boolean isPrivate = false;
	int mostRecentPlayed = 0;

	public void follow(User user){
		following.add(user);
		user.followers.add(this);
	}
	public void unfollow(User user){
		following.remove(user);
		user.followers.remove(this);
	}
	@Override public JsonObject serialize(User user,Type type,JsonSerializationContext context) {
		JsonObject json = super.serialize(user,type,context);
		json.addProperty("lid",   user.library.id);
		json.addProperty("fname", user.fname);
		json.addProperty("lname", user.lname);
		json.addProperty("image", user.image);
		json.addProperty("accountType",user.accountType.toString());
		json.addProperty("numFollowers",user.followers.size());
		json.add("albumIds",toJsonIdArray(user.playlists));
		return json;
	}
	public static User find(int id){
		return find(User.class,id);
	}
	public static User find(Request request,Response response){
		return find(User.class, request, response);
	}
	public static String login(Request request,Response response){
		response.status(HTTP_UNAUTHORIZED);
		try{
			Credentials creds = fromJson(request.body(),Credentials.class);
			TypedQuery<User> query = FINDER.createQuery("Select u from User u where u.email = :e",User.class);
			query.setParameter("e",creds.email);
			User user = query.getSingleResult();
			if(user.isBanned){
				response.status(HTTP_FORBIDDEN);
				return "";
			}
			if(user.passwordHash.equals(hash(creds.password,user.salt))){
				Session session = request.session(true);
				session.attribute("UID",user.id);
				response.status(HTTP_OK);
				return toJson(user);
			}
			return"";
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String logout(Request request,Response response){
		Session session = request.session(false);
		if(session != null) session.invalidate();
		return"";
	}
	public static User create(Request request,Response response){
		Info info = fromJson(request.body(),Info.class);
		if(info==null) throw halt(HTTP_BAD_REQUEST);
		User user = info.user;
		user.email = info.credentials.email;
		user.salt = newSalt();
		user.passwordHash = hash(info.credentials.password,user.salt);
		user.subscription = info.subscription;
		user.bankAccount = info.bankAccount;
		if(!user.persist()) throw halt(HTTP_CONFLICT);
		response.status(HTTP_CREATED);
		return user;
	}
	public static String delete(Request request, Response response){
		response.status(HTTP_NOT_FOUND);
		try(User user = find(Requests.getId(request))){
			user.remove();
			response.status(HTTP_OK);
			return"";
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String getPlaylists(Request request,Response response){
		response.status(HTTP_NOT_FOUND);
		try(User user = find(Requests.getId(request))){
			String json = toJson(user.playlists);
			response.status(HTTP_OK);
			return json;
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String upgradeAccount(Request request, Response response){
		response.status(HTTP_BAD_REQUEST);
		try(User user = find(Requests.getCurrentUID(request))){
			Subscription sub = fromJson(request.body(), Subscription.class);
			if(sub != null){
				user.subscription = sub;
				user.accountType = AccountType.PREMIUM;
				user.merge();
				response.status(HTTP_OK);
				return"";
			}
			return"";
		}catch(RuntimeException e){
			e.printStackTrace();
			return"";
		}
	}
	public static String downgradeAccount(Request request, Response response){
		response.status(HTTP_BAD_REQUEST);
		try(User user = find(Requests.getCurrentUID(request))){
			user.subscription = new Subscription();
			user.accountType = AccountType.BASIC;
			user.merge();
			response.status(HTTP_OK);
			return"";
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String ban(Request request, Response response){
		response.status(HTTP_BAD_REQUEST);
		try(User user = find(Requests.getId(request))){
			user.isBanned = true;
			user.merge();
			response.status(HTTP_OK);
			return"";
		}catch(RuntimeException e){
			return"";
		}
	}
	public static String updatePassword(Request request, Response response){
		response.status(HTTP_INTERNAL_ERROR);
		Password passObj = fromJson(request.body(), Password.class);
		if(passObj != null){
			try(User user = find(Requests.getCurrentUID(request))){
				if(user.passwordHash.equals(hash(passObj.oldPassword,user.salt))){
					user.passwordHash = hash(passObj.newPassword,user.salt);
					user.merge();
					response.status(HTTP_OK);
					return"";
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return"";
	}
}