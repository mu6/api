package mu6;

import com.google.gson.*;
import spark.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.*;
import java.lang.reflect.*;
import java.sql.*;
import java.util.*;
import static java.net.HttpURLConnection.*;
import static mu6.Json.toJson;
import static mu6.Requests.getId;
import static mu6.Transaction.FACTORY;
import static spark.Spark.halt;

@MappedSuperclass public abstract class DatabaseEntity<T extends DatabaseEntity<T>>implements Serializable,Closeable,Comparable,JsonSerializer<T>{
	@Id @GeneratedValue(strategy=GenerationType.IDENTITY) int id;
	@NotNull String title = "";
	@NotNull Timestamp date = new Timestamp(System.currentTimeMillis());

	@Deprecated @Override public void close(){}

	@Override public final String toString(){//lets us return DatabaseEntities directly from our request-handling methods
		return toJson(this);
	}
	@Override public final boolean equals(Object o){
		return o instanceof DatabaseEntity && id==((DatabaseEntity)o).id;
	}
	@Override public final int hashCode(){
		return id;
	}
	@Override public final int compareTo(Object o){
		return o instanceof DatabaseEntity?Integer.compare(id,((DatabaseEntity)o).id):-1;
	}
	@Override public JsonObject serialize(T t,Type type,JsonSerializationContext context){
		JsonObject json = new JsonObject();
		json.addProperty("id",t.id);
		json.addProperty("title",t.title);
		String date = t.date.toString();
		json.addProperty("releaseDate",date);//albums & tracks
		json.addProperty("joined",date);//users
		json.addProperty("time",date);//concerts?
		return json;
	}
	//an EntityManager used exclusively for finding and querying, not transactions
	public static final EntityManager FINDER = FACTORY.createEntityManager();

	public static<T extends DatabaseEntity<T>>T find(Class<T>clas,int id){
		return FINDER.find(clas,id);
	}
	public static<T extends DatabaseEntity<T>>T find(Class<T>clas,Request request,Response response){
		final int id = getId(request);
		T entity = find(clas,id);
		if(entity==null) throw halt(HTTP_NOT_FOUND);
		response.status(HTTP_OK);
		return entity;
	}
	public static<T extends DatabaseEntity<T>>List<T>query(String query,Class<T>clas,Object...args){
		TypedQuery<T>q = FINDER.createQuery(query,clas);
		for(int i=0;i<args.length;++i) q.setParameter(i,args[i]);
		return q.getResultList();
	}
	public static<T extends DatabaseEntity<T>>List<T>query(int max,String query,Class<T>clas,Object...args){
		TypedQuery<T>q = FINDER.createQuery(query,clas).setMaxResults(max);
		for(int i=0;i<args.length;++i) q.setParameter(i,args[i]);
		return q.getResultList();
	}
	public static Transaction detach (DatabaseEntity e){return new Transaction().detach(e);}
	public static Transaction merge  (DatabaseEntity e){return new Transaction().merge(e);}
	public static Transaction persist(DatabaseEntity e){return new Transaction().persist(e);}
	public static Transaction refresh(DatabaseEntity e){return new Transaction().refresh(e);}
	public static Transaction remove (DatabaseEntity e){return new Transaction().remove(e);}
	public static Transaction run(Runnable e){return new Transaction().run(e);}
	public boolean detach (){return detach (this).committed();}
	public boolean merge  (){return merge  (this).committed();}
	public boolean persist(){return persist(this).committed();}
	public boolean refresh(){return refresh(this).committed();}
	public boolean remove (){return remove (this).committed();}
}