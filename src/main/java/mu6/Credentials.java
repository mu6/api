package mu6;

import org.bouncycastle.crypto.generators.*;
import java.security.*;
import java.util.*;

public class Credentials{
	private static final int N = 16384;
	private static final int r = 8;
	private static final int p = 1;
	private static final int dkLen = 64;

	public static String hash(String password,String salt){
		return Base64.getEncoder().encodeToString(SCrypt.generate(password.getBytes(),salt.getBytes(),N,r,p,dkLen));
	}
	public static String newSalt(){
		byte[] saltBytes = new byte[64];
		new SecureRandom().nextBytes(saltBytes);
		return Base64.getEncoder().encodeToString(saltBytes);
	}
	public String email = "";
	public String password = "";
}