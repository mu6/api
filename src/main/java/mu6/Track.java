package mu6;

import com.google.gson.*;
import spark.*;
import java.lang.reflect.*;
import java.util.*;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static java.net.HttpURLConnection.*;
import static javax.persistence.CascadeType.MERGE;
import static mu6.Json.*;
import static mu6.Requests.processMultipartRequest;
import static spark.Spark.halt;

@Entity public class Track extends DatabaseEntity<Track>{
	public static enum Genre{
		POP,ROCK,COUNTRY,HIPHOP,INDIE
	}
	long playCount = 0;
	@NotNull String path = "";
	@NotNull String lyrics = "";
	@NotNull Genre genre = Genre.POP;
	@NotNull @ManyToOne(cascade=MERGE)Playlist album = new Playlist();

	public void setAlbum(Playlist album){
		this.album.remove(this);
		this.album = album;
		this.album.add(this);
	}
	@Override public JsonObject serialize(Track track,Type type,JsonSerializationContext context){
		JsonObject json = super.serialize(track,type,context);
		json.addProperty("path",     track.path);
		json.addProperty("genre",    track.genre.toString());
		json.addProperty("lyrics",   track.lyrics);
		json.addProperty("albumId",  track.album.id);
		json.addProperty("artistId", track.album.owner.id);
		json.addProperty("playCount",track.playCount);
		return json;
	}
	public static Track find(int id){return find(Track.class,id);}
	public static Track find(Request request,Response response){return find(Track.class,request,response);}
	public static List<Track>query(String query,Object...args){return query(query,Track.class,args);}
	public static List<Track>query(int max,String query,Object...args){return query(max,query,Track.class,args);}

	public static Track create(Request request,Response response){
		Metadata data = processMultipartRequest(request);
		Playlist playlist = Playlist.find(data.albumId);
		if(playlist==null) throw halt(HTTP_BAD_REQUEST);
		Track track = new Track();
		track.setAlbum(playlist);
		track.title = data.title;
		track.lyrics = data.lyrics;
		track.date = data.date;
		track.path = data.path;
		if(!persist(track).merge(playlist).committed()) throw halt(HTTP_CONFLICT);
		response.status(HTTP_CREATED);
		return track;
	}
	public static String getTopTracks(Request request,Response response){
		response.status(HTTP_OK);
		int MAX_SONGS = 3;
		String query="SELECT t from Track t ORDER BY t.playCount DESC";
		return toJson(FINDER.createQuery(query,Track.class).setMaxResults(MAX_SONGS).getResultList());
	}
	public static String updatePlayCount(Request request, Response response){
		response.status(HTTP_INTERNAL_ERROR);
		try{
			Track track = find(Requests.getId(request));
			++track.playCount;
			try{
				User user = User.find(Requests.getCurrentUID(request));
				if(!user.isPrivate){
					user.mostRecentPlayed = track.id;
				}
				response.status(HTTP_CREATED);
				return"";
			}catch(Exception e){
				response.status(HTTP_UNAUTHORIZED);
				return"";
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return"";
	}
}