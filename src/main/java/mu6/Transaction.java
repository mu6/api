package mu6;

import javax.persistence.*;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.*;
import java.io.*;
import java.util.*;

public class Transaction implements Closeable,EntityManager,EntityTransaction{
	public static final EntityManagerFactory FACTORY = Persistence.createEntityManagerFactory("mu6");
	private final EntityManager e = FACTORY.createEntityManager();
	public RuntimeException x = null;
	public Transaction(){
		begin();
	}
	public Transaction detach(DatabaseEntity e){
		if(x == null) try{
			detach((Object)e);
		}catch(RuntimeException r){
			x = r;
		}
		return this;
	}
	public Transaction merge(DatabaseEntity e){
		if(x == null) try{
			merge((Object)e);
		}catch(RuntimeException r){
			x = r;
		}
		return this;
	}
	public Transaction persist(DatabaseEntity e){
		if(x == null) try{
			persist((Object)e);
		}catch(RuntimeException r){
			x = r;
		}
		return this;
	}
	public Transaction refresh(DatabaseEntity e){
		if(x != null) try{
			refresh((Object)e);
		}catch(RuntimeException r){
			x = r;
		}
		return this;
	}
	public Transaction remove(DatabaseEntity e){
		if(x == null) try{
			remove((Object)e);
		}catch(RuntimeException r){
			x = r;
		}
		return this;
	}
	public Transaction run(Runnable e){
		if(x == null) try{
			e.run();
		}catch(RuntimeException r){
			x = r;
		}
		return this;
	}
	public boolean committed(){
		try{
			if(x != null) return false;
			commit();
			return true;
		}catch(RuntimeException r){
			x = r;
			return false;
		}finally{
			close();
		}
	}
	public boolean commitOrThrow(){
		try{
			if(x != null) throw x;
			commit();
			return true;
		}catch(RuntimeException r){
			throw x = r;
		}finally{
			close();
		}
	}
	@Override public void close(){
		e.close();
	}
	//BOILERPLATE ENTITYTRANSACTION DELEGATION METHODS BELOW
	@Override public void begin(){getTransaction().begin();}
	@Override public void commit(){getTransaction().commit();}
	@Override public boolean getRollbackOnly(){return getTransaction().getRollbackOnly();}
	@Override public boolean isActive(){return getTransaction().isActive();}
	@Override public void rollback(){getTransaction().rollback();}
	@Override public void setRollbackOnly(){getTransaction().setRollbackOnly();}
	//BOILERPLATE ENTITYMANAGER DELEGATION METHODS BELOW
	@Override public void persist(Object o){e.persist(o);}
	@Override public<T>T merge(T t){return e.merge(t);}
	@Override public void remove(Object o){e.remove(o);}
	@Override public<T>T find(Class<T>c,Object o){return e.find(c,o);}
	@Override public<T>T find(Class<T>c,Object o,Map<String,Object> m){return e.find(c,o,m);}
	@Override public<T>T find(Class<T>c,Object o,LockModeType t){return e.find(c,o,t);}
	@Override public<T>T find(Class<T>c,Object o,LockModeType t,Map<String,Object>m){return e.find(c,o,t,m);}
	@Override public<T>T getReference(Class<T>c,Object o){return e.getReference(c,o);}
	@Override public void flush(){e.flush();}
	@Override public void setFlushMode(FlushModeType t){e.setFlushMode(t);}
	@Override public FlushModeType getFlushMode(){return e.getFlushMode();}
	@Override public void lock(Object o,LockModeType t){e.lock(o,t);}
	@Override public void lock(Object o,LockModeType t,Map<String,Object>m){e.lock(o,t,m);}
	@Override public void refresh(Object o){e.refresh(o);}
	@Override public void refresh(Object o,Map<String,Object>m){e.refresh(o,m);}
	@Override public void refresh(Object o,LockModeType t){e.refresh(o,t);}
	@Override public void refresh(Object o,LockModeType t,Map<String,Object>m){e.refresh(o,t,m);}
	@Override public void clear(){e.clear();}
	@Override public void detach(Object o){e.detach(o);}
	@Override public boolean contains(Object o){return e.contains(o);}
	@Override public LockModeType getLockMode(Object o){return e.getLockMode(o);}
	@Override public void setProperty(String s,Object o){e.setProperty(s,o);}
	@Override public Map<String,Object>getProperties(){return e.getProperties();}
	@Override public Query createQuery(String s){return e.createQuery(s);}
	@Override public<T>TypedQuery<T>createQuery(CriteriaQuery<T> c){return e.createQuery(c);}
	@Override public Query createQuery(CriteriaUpdate c){return e.createQuery(c);}
	@Override public Query createQuery(CriteriaDelete c){return e.createQuery(c);}
	@Override public<T>TypedQuery<T>createQuery(String s,Class<T>c){return e.createQuery(s,c);}
	@Override public Query createNamedQuery(String s){return e.createNamedQuery(s);}
	@Override public<T>TypedQuery<T>createNamedQuery(String s,Class<T>c){return e.createNamedQuery(s,c);}
	@Override public Query createNativeQuery(String s){return e.createNativeQuery(s);}
	@Override public Query createNativeQuery(String s,Class c){return e.createNativeQuery(s,c);}
	@Override public Query createNativeQuery(String s,String c){return e.createNativeQuery(s,c);}
	@Override public StoredProcedureQuery createNamedStoredProcedureQuery(String s){return e.createNamedStoredProcedureQuery(s);}
	@Override public StoredProcedureQuery createStoredProcedureQuery(String s){return e.createStoredProcedureQuery(s);}
	@Override public StoredProcedureQuery createStoredProcedureQuery(String s,Class...c){return e.createStoredProcedureQuery(s,c);}
	@Override public StoredProcedureQuery createStoredProcedureQuery(String s,String...c){return e.createStoredProcedureQuery(s,c);}
	@Override public void joinTransaction(){e.joinTransaction();}
	@Override public boolean isJoinedToTransaction(){return e.isJoinedToTransaction();}
	@Override public<T>T unwrap(Class<T>c){return e.unwrap(c);}
	@Override public Object getDelegate(){return e.getDelegate();}
	@Override public boolean isOpen(){return e.isOpen();}
	@Override public EntityTransaction getTransaction(){return e.getTransaction();}
	@Override public EntityManagerFactory getEntityManagerFactory(){return e.getEntityManagerFactory();}
	@Override public CriteriaBuilder getCriteriaBuilder(){return e.getCriteriaBuilder();}
	@Override public Metamodel getMetamodel(){return e.getMetamodel();}
	@Override public<T>EntityGraph<T>createEntityGraph(Class<T>c){return e.createEntityGraph(c);}
	@Override public EntityGraph<?>createEntityGraph(String s){return e.createEntityGraph(s);}
	@Override public EntityGraph<?>getEntityGraph(String s){return e.getEntityGraph(s);}
	@Override public<T>List<EntityGraph<?super T>>getEntityGraphs(Class<T>c){return e.getEntityGraphs(c);}
}