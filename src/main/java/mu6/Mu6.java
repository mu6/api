package mu6;

import static spark.Spark.*;

public class Mu6{
	public static void main(String...args){
		options("/*", (request, response) -> {
			String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
			if (accessControlRequestHeaders != null) {
				response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
			}
			String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
			if (accessControlRequestMethod != null) {
				response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
			}
			return "";
		});
		//static URLs must be placed before parameterized URLs
		get("/logout",User::logout);
		post("/login",User::login);
		post("/user/add",User::create);
		post("/track/add",Track::create);
		post("/album/add",Playlist::create);
		post("/concert/add", Concert::create);
		post("/user/upgrade", User::upgradeAccount);
		get("/user/downgrade", User::downgradeAccount);
		post("/user/update/password", User::updatePassword);
		get("/track/getNSongs", Track::getTopTracks);
		delete("/user/remove/:id", User::delete);
		get("/user/ban/:id", User::ban);
		post("track/played/:id", Track::updatePlayCount);
		get("/user/:id",User::find);
		get("/artist/:id",User::find);
		get("/track/:id",Track::find);
		get("/album/:id",Playlist::find);
		get("/user/:id/playlists",User::getPlaylists);
		get("/user/:id/library",Playlist::getTracks);
		get("/list/:lid/:tid",Playlist::addTrack);
		delete("/list/:lid/:tid",Playlist::removeTrack);
		get("/list/:id",Playlist::getTracks);
		delete("list/:lid",Playlist::delete);
		before((request,response)->response.header("Access-Control-Allow-Origin","*"));
	}
}