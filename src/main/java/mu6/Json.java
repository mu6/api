package mu6;

import com.google.gson.*;

import java.util.*;

public interface Json{
	Gson GSON = new GsonBuilder()
		.disableHtmlEscaping()
		.setDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
		.registerTypeAdapter(User.class,new User())
		.registerTypeAdapter(Track.class,new Track())
		.registerTypeAdapter(Playlist.class,new Playlist())
		.registerTypeAdapter(Concert.class,new Concert())
		.create();

	static String toJson(Object o){
		return GSON.toJson(o);
	}
	static<T>T fromJson(String json,Class<T>clas){
		try {
			return GSON.fromJson(json, clas);
		}catch(JsonSyntaxException e){
			return null;
		}
	}
	static JsonArray toJsonIdArray(Collection<?extends DatabaseEntity>c){
		return GSON.toJsonTree(c.stream().mapToInt(e->e.id).toArray()).getAsJsonArray();
	}
}