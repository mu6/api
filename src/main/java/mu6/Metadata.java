package mu6;

import java.sql.Timestamp;

public class Metadata{
	public String title = "";
	public String description = "";
	public String lyrics = "";
	public String path = "";
	public String venue = "";
	public Timestamp date = new Timestamp(System.currentTimeMillis());
	public int albumId;
	public int artistId;
}